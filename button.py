# This is the control button class that support restarting game and show win or lose. 

import pygame
from mouselistener import MouseListener
from metabutton import MetaButton

class ControlButton(MetaButton, pygame.sprite.Sprite):

	def __init__(self, pos, width, height):
		"""
		Button should react when clicked. When clicked, restart the game
		When the game is running, the button should show a smiling face.
		When the player wins the game, the button shows a laughing face.
		When the player lose the game, the button shows a dead face.
		"""
		"""
	   Button show load three images of faces.
	   When clicked, tell MineSweeper class to restart the game.
	   """
		pygame.sprite.Sprite.__init__(self)
		self.mouse = MouseListener()
		self.__width = width
		self.__height = height
		self.__pos = pos
		self.smile_img = pygame.transform.smoothscale(pygame.image.load("images/smile.png").convert_alpha(),(self.__width, self.__height))
		self.smile_click_img = pygame.transform.smoothscale(pygame.image.load("images/smile_click.png").convert_alpha(),
														(self.__width, self.__height))
		self.laugh_img = pygame.transform.smoothscale(pygame.image.load("images/laugh.png").convert_alpha(),(self.__width, self.__height))
		self.laugh_click_img = pygame.transform.smoothscale(pygame.image.load("images/laugh_click.png").convert_alpha(),
														(self.__width, self.__height))
		self.dead_img = pygame.transform.smoothscale(pygame.image.load("images/dead.png").convert_alpha(),(self.__width, self.__height))
		self.dead_click_img = pygame.transform.smoothscale(pygame.image.load("images/dead_click.png").convert_alpha(),
														(self.__width, self.__height))
		self.image = self.smile_img
		self.rect = self.__pos

	def x(self):
		return self.__pos[0]

	def y(self):
		return self.__pos[1]

	def width(self):
		return self.__width

	def height(self):
		return self.__height

	def leftclick(self):
		if MouseListener.isLeftKeyClicked(self):
			self.image = self.smile_click_img
		else:
			pygame.time.wait(100)
			self.smile()

	def midclick(self):
		pass

	def rightclick(self):
		pass

	def smile(self):
		self.image = self.smile_img

	def laugh(self):
		self.image = self.laugh_img

	def dead(self):
		self.image = self.dead_img
