# This is an abstract class to define a button interface 

from abc import ABCMeta, abstractmethod

class MetaButton():
	 
    __metaclass__ = ABCMeta
	
    @abstractmethod
    def leftclick(self):
        pass

    @abstractmethod
    def midclick(self):
        pass

    @abstractmethod
    def rightclick(self):
        pass