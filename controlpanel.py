# This class contains a mine counter, a timer, and a restart button.

import pygame
from mouselistener import MouseListener
from button import ControlButton
from timer import Timer
from counter import Counter


class ControlPanel(object):

	def __init__(self):

		self.mineCounter = Counter()
		self.timer = Timer()
		self.button = ControlButton()
		self.mouse_click = MouseListener()

	def countMine(self):
		pass

	def countTime(self):
		pass

	def stopTimer(self):
		pass

	def reduceMineCount(self):
		self.mineCounter.decreaseOne()

	def addMineCount(self):
		self.mineCounter.addOne()

	def showDeadFace(self):
		self.button.dead()

	def showHappyFace(self):
		self.button.laugh()

	def restartClicked(self):
		if self.button.leftclick():
			# Game restart
			self.mineCounter.reset()
			self.timer.reset()
			self.button.smile()