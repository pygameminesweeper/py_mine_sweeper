# This class generates a mine field layout.

import random
from cell import Cell

class FieldGenerator(object):

	@classmethod
	def init(cls, width, height, total_mine):
		mine_num = total_mine
		width = width
		height = height
		field = [[0 for row in xrange(0, width)] for column in xrange(0, height)]
		mine_pos = []
	
	@classmethod
	def generate(cls, click_pos):
		"""Generate a field by width and height. Set the clicked cell to empty."""
		
		# Randomly place mine
		cls.__placeMine(click_pos)
		
		# Then randomly generate hints around the mines
		cls.__calcHints()

		return cls.field

	@classmethod		
	def __placeMine(cls, click_pos):
		x = 0
		y = 1

		# Place mines randomly on the map
		for i in xrange(0, cls.mine_num):
			m_coord = cls.__randCoord(width, height)
			real_coord = cls.__logicCoordToRealCoord(m_coord)
			if not (m_coord ==click_pos):
				cls.field[real_coord[x]][real_coord[y]] = Cell.CONST_MINE_VAL
				cls.mine_pos.append(real_coord)

	@classmethod
	def __calcHints(cls):
		"""
		for(int y = 0; y < mHeight; y ++) {
			for(int x = 0; x < mWidth; x ++) {
				if(mMinefield[y][x] != '*') {
					mMinefield[y][x] = minesNear(y, x);
				}
			}
		}
		"""
		x = 0 
		y = 1

		for i in xrange(0, cls.width):
			for j in xrange(0, cls.height):
				real_coord = cls.__logicCoordToRealCoord((i,j))
				if (not cls.__isMine(real_coord)):
					field[real_coord[x]][real_coord[y]] = cls.__countNearbyMines(real_coord)

	@classmethod
	def __countNearbyMines(cls, real_coord):
		mines = 0
		logic_coord = cls.__realCoordToLogicCoord(real_coord)
		
		n_coord = cls.__logicCoordToRealCoord(cls.__North(logic_coord))
		mines += cls.__mineAt(n_coord)

		s_coord = cls.__logicCoordToRealCoord(cls.__South(logic_coord))
		mines += cls.__mineAt(s_coord)

		e_coord = cls.__logicCoordToRealCoord(cls.__East(logic_coord))
		mines += cls.__mineAt(e_coord)

		w_coord = cls.__logicCoordToRealCoord(cls.__West(logic_coord))
		mines += cls.__mineAt(w_coord)

		nw_coord = cls.__logicCoordToRealCoord(cls.__NW(logic_coord))
		mines += cls.__mineAt(nw_coord)

		ne_coord = cls.__logicCoordToRealCoord(cls.__NE(logic_coord))
		mines += cls.__mineAt(ne_coord)

		se_coord = cls.__logicCoordToRealCoord(cls.__SE(logic_coord))
		mines += cls.__mineAt(se_coord)

		sw_coord = cls.__logicCoordToRealCoord(cls.__SW(logic_coord))
		mines += cls.__mineAt(sw_coord)

		return mines


	@classmethod
	def __randCoord(cls, width, height):
		"""Return a random integer coodinate"""
		return (random.randint(0,width),random.randint(0,height))

	@classmethod
	def __isMine(cls,real_coord):
		return (real_coord in cls.field)

	@classmethod
	def __mineAt(cls, real_coord):
		if (real_coord in cls.field):
			return 1
		else:
			return 0

	@classmethod
	def __North(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[y] == 0:
			return logic_coord
		else:
			return (logic_coord[x], logic_coord[y]-1)

	@classmethod
	def __South(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[y] == cls.height:
			return logic_coord
		else:
			return (logic_coord[x], logic_coord[y]+1)

	@classmethod
	def __West(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[x] == 0:
			return logic_coord
		else:
			return (logic_coord[x] - 1, logic_coord[y])

	@classmethod
	def __East(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[x] == cls.width:
			return logic_coord
		else:
			return (logic_coord[x] + 1, logic_coord[y])

	@classmethod
	def __NW(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[x] == 0 or logic_coord[y] == 0:
			return logic_coord
		else:
			return (logic_coord[x] - 1, logic_coord[y] - 1)
	
	@classmethod
	def __NE(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[x] == width or logic_coord[y] == 0:
			return logic_coord
		else:
			return (logic_coord[x] + 1, logic_coord[y] - 1)

	@classmethod
	def __SW(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[x] == 0 or logic_coord[y] == height :
			return logic_coord
		else:
			return (logic_coord[x] - 1, logic_coord[y] + 1)

	@classmethod
	def __SE(cls, logic_coord):
		x = 0
		y = 1

		if logic_coord[x] == cls.width or logic_coord[y] == cls.height:
			return logic_coord
		else:
			return (logic_coord[x] + 1, logic_coord[y] + 1)


	@classmethod
	def __logicCoordToRealCoord(cls, logic_coord):
		"""
		2d array access column first, then access row. So a conversion is needed
		"""
		x = 0
		y = 1

		return (logic_coord[y],logic_coord[x])

	@classmethod
	def __realCoordToLogicCoord(cls, real_coord):
		"""
		2d array access column first, then access row.
		Convert back to logic coordinate to maintain consistency.
		"""
		x = 0
		y = 1

		return (real_coord[y],real_coord[x])

	@classmethod
	def __str__(cls):
		return str(cls.field)

	"""
	public char minesNear(int y, int x) {
		int mines = 0;
		// check mines in all directions
		mines += mineAt(y - 1, x - 1);  // NW
		mines += mineAt(y - 1, x);      // N
		mines += mineAt(y - 1, x + 1);  // NE
		mines += mineAt(y, x - 1);      // W
		mines += mineAt(y, x + 1);      // E
		mines += mineAt(y + 1, x - 1);  // SW
		mines += mineAt(y + 1, x);      // S
		mines += mineAt(y + 1, x + 1);  // SE
		if(mines > 0) {
		  // we're changing an int to a char
		  // why?!
		  // http://www.asciitable.com/
		  // 48 is ASCII code for '0'
		  return (char)(mines + 48);
		} else {
		  return ' ';
		}
	  }

	  // returns 1 if there's a mine a y,x or 0 if there isn't
	  public int mineAt(int y, int x) {
		// we need to check also that we're not out of array bounds as that would
		// be an error
		if(y >= 0 && y < mHeight && x >= 0 && x < mWidth && mMinefield[y][x] == '*') {
		  return 1;
		} else {
		  return 0;
		}
	  }

	"""
