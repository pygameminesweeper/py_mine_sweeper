# This class defines the game rule of minesweeper and runs the game loop

import pygame
from grid import Grid


class GameRule(object):

	@classmethod
	def init(cls):
		win = False
		lose = False
		

	@classmethod
	def applyRule():
		"""
		Get the last clicked cell
		Check what type of cell is clicked:
			1. Mine -> You are dead. End of Game
			2. Number cell -> ask cell to draw its revealed state (and thus display its value on screen)
			3. Empty cell -> reveal all its adjacent non-mine cell using BFS or DFS
		Check whether a flag is raised
			1. If yes, lock the cell, and reduce mine count on control panel by 1
			2. Ask cell to draw its updated state
		Check whether a flag is down
			1. If yes, unlock the cell and ask cell to draw its updated state
		"""
		lastClickedCell = Grid.getLastClickedCell()

		if lastClickedCell.isRevealed():			
			if lastClickedCell.isMine():
				cls.lose = True
			
			elif lastClickedCell.isHint():
				lastClickedCell.draw()
			
			elif lastClickedCell.isEmptyCell()
				Grid.revealNeighbors(lastClickedCell)
		
		elif lastClickedCell.raisedFlag():
			lastClickedCell.lock()
			lastClickedCell.draw()
			cp.reduceMineCount(1)
		
		elif lastClickedCell.downedFlag():
			lastClickedCell.unlock()
			lastClickedCell.draw()
			cp.addMineCount()
		
		elif Grid.cellCleared() or Grid.allMineFlagged():
			cls.win = True
		
		else:
			pass


