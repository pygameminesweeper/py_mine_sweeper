# This class handles all the cells in minesweeper.

import pygame
from cell import Cell
from mouselistener import MouseListener

class Grid(object):
	"""

	"""
	@classmethod
	def init(cls, width, height, cell_width, cell_height):
		"""
		Grid class should handle all cells.
		Use sprite class to contain them. 
		Grid receives click position and map the position to the clicked cell.
		Then ask the cell to change itcls to clicked state.
		Grid should keep track of the last clicked cell, and maintain two booleans
		revealed and flagged to indicate the action performed on the last clicked cell.
		"""
		cls.cell_width = cell_width
		cls.cell_height = cell_height

		# Load image resources
		cls.cell_covered_img = pygame.transform.smoothscale(pygame.image.load("images/covered.png").convert_alpha(),(cls.size, cls.size))
		cls.cell_flagged_img = pygame.transform.smoothscale(pygame.image.load("images/flagged.png").convert_alpha(),(cls.size, cls.size))
		cls.mine_img = pygame.transform.smoothscale(pygame.image.load("images/mine.png").convert_alpha(),(cls.size, cls.size))
		cls.exploded_img = pygame.transform.smoothscale(pygame.image.load("images/exploded_mine.png").convert_alpha(),(cls.size, cls.size))
		cls.empty_tile_img = pygame.transform.smoothscale(pygame.image.load("images/empty_tile.png").convert_alpha(),(cls.size, cls.size))
		cls.hint_img_lst = [pygame.transform.smoothscale(pygame.image.load("images/"+i+".png").convert_alpha(),(cls.size, cls.size)) for i in xrange(1,5)]

		# Initialize cell field
		field = [[Cell(pos, (w,h), size, covered_img, empty_tile_img, flag_img, mine_img, explode_img, hint_img_lst)\
                        for w in xrange(0,width/cell_width)] for h in xrange(0,height/cell_height)]
		
		MouseListener.init()        

		# Initialize this entry when received the first click
		lastClickedCell = Cell() 

		pass

	"""
	Write a method that receives click position and click event type.
	Store the information in this class and send it to the clicked cell.
	"""
	@classmethod
	def receiveClickEvent(cls):
		"""Call MouseListener here to receive position and type"""
		click_event = MouseListener.getClickEvent()

	@classmethod
	def getLastClickedCell(cls):
		if revealed:
			return lastClickedCell

	@classmethod
	def northOf(cls, cell):
		internPos = cell.getInternPos()
		if internPos[1] == 0:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]
	
	@classmethod
	def southOf(cls, cell):
		internPos = cell.getInternPos()
		if internPos[1] == cls.height:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]

	@classmethod
	def westOf(cls, cell):
		internPos = cell.getInternPos()
		if internPos[0] == 0:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]
	
	@classmethod
	def eastOf(cls, cell):
		internPos = cell.getInternPos()
		if internPos[0] == cls.width:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]

	@classmethod
	def northeast(cls, cell):
		internPos = cell.getInternPos()
		if internPos[0] == cls.width or internPos[1] == 0:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]


	@classmethod
	def southeast(cls,cell):
		internPos = cell.getInternPos()
		if internPos[0] == cls.width or internPos[1] == cls.height:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]

	@classmethod
	def northwest(cls,cell):
		internPos = cell.getInternPos()
		if internPos[0] == 0 or internPos[1] == 0:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]

	@classmethod
	def southwest(cls,cell):
		internPos = cell.getInternPos()
		if internPos[0] == 0 or internPos[1] == cls.height:
			return cell
		else:
			return field[internPos[0]-1][internPos[1]]

	@classmethod
	def neighborOf(cls, cell):
		return [cls.northOf(cell), \
				cls.southOf(cell), \
				cls.westOf(cell), \
				cls.eastOf(cell), \
				cls.northwest(cell), \
				cls.northeast(cell), \
				cls.southwest(cell), \
				cls.southeast(cell)]

	@classmethod
	def revealNeighbors(cls, clickedCell):
		for cell in cls.neighborOf(clickedCell):
			if not cell.isMine():
				cell.leftclick()

	@classmethod
	def revealMines(cls):
		"""Reveal all mines to the player if the play loses the game."""
		pass

	@classmethod
	def lockAllCells(cls):
		"""Make all cells unclickable"""
		for row in field:
			for cell in row:
				cell.lock()

	@classmethod
	def unlockAllCells(cls):
		"""Make all cells clickable"""
		for row in field:
			for cell in row:
				cell.unlock()


	@classmethod
	def setAllCells(cls):
		"""Set the values of all cells using field generator."""
		field

	@classmethod
	def internalPosToAbsPos(cls, internalPos):
		x = float(internalPos[0])
		y = float(internalPos[1])
		x = x * cls.cell_width
		y = y * cls.cell_height
		return (x, y)

	@classmethod
	def absPosToInternalPos(cls, absPos):
		x = float(absPos[0])
		y = float(absPos[1])
		x = round(x / float(cls.cell_width))
		y = round(y / float (cls.cell_height))
		return (x, y)
