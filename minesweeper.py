import pygame
from gamerule import GameRule
from grid import Grid
from controlpanel import ControlPanel
		
class MineSweeper(object):
    
    width = 640
    height = 480
    my_win = pygame.display.set_mode((width, height))

	@classmethod
	def init(cls):
		cp = ControlPanel() # The part that holds counter, timer, and restart button
		Grid.init(width, height, cell_width, cell_height)
		pass

	@classmethod
	def rungame(cls):
		GameRule.init()

		"""Put the game to sleep until a click is detected."""
		# Keep applying rule to monitor game state
		while (not GameRule.win) or (not GameRule.lose):
			"""Not real code here. Need to find a proper implementation."""
			if MouseListener.clicked():
				GameRule.applyRule()
			else
				pygame.sleep()

		if GameRule.win:
			Grid.lockAllCells()
			cp.stopTimer()
			cp.showHappyFace()
			cls.displayWinMessage()
		elif GameRule.lose:
			Grid.lockAllCells()
			Grid.revealMines()
			cp.stopTimer()
			cp.showDeadFace()
			cls.displayLoseMessage()
		elif cp.restartClicked():
			cls.restartGame()
		else:
			pass

	@classmethod
	def displayWinMessage(cls):
		"""
		Pop a window to congratulate the player
		"""
		pass

	@classmethod
	def displayLoseMessage(cls):
		"""
		Pop a window to tell the player that the game is lost
		"""
		pass

	@classmethod
	def restartGame(cls):
		"""
		Restart the game by asking Grid to reinitialize all cells. 
		Ask ControlPanel to reinitiate itself
		"""
		pass





