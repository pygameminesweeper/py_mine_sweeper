
from cell import Cell
from grid import Grid

class GeneralAlgorithm():

    def __init__(self, row, col):
        # row and col represent the valid cells' index
        self.__row = row
        self.__col = col

    def depth_first_search(self, cell):
        grid = Grid()
        if cell.getInternPos() < (0, 0) or cell.getInternPos() > (self.__row, self.__col): # out of bound
            return False
        else:
            if not cell.isRevealed(): # uncovered
                return False
            else:
                cell.leftclick() # uncover the cell
                if cell.isMine():
                    return True
                else:
                    for c in grid.neighborOf(cell):
                        if c.isMine(): # if cell's neighbour is a Mine
                            return False
                        else: # checking neighbours recursively
                            self.depth_first_search(c)

    def breath_first_search(self, cell):
        if (not cell.isRevealed()) or\
                (cell.getInternPos() < (0, 0) or cell.getInternPos() > (self.__row, self.__col)):
            return False
        else:
            to_search = [cell.getInternPos()]
            while to_search.__len__() != 0:
                cell = to_search[0]
                to_search = to_search[1::]
                if cell.isRevealed():
                    cell.leftclick()
                    if cell.isMine():
                        return True
                    else:
                        grid = Grid()
                        for c in grid.neighborOf(cell):
                            if not c.isMine():
                                to_search += c
                                # to end of to_search
            return False
