# The mouse listener of the game.
# Checks the mouse-button events and records the clicking position.
# @author Steve Lan, Yunmeng Li
# @version Winter, 2017

import pygame
# import button


class MouseListener:

    """
    MouseListener listens mouse click. If a click event happens, sends the click position
    and click event type (left, mid, right).
    """
    CONST_LEFT_KEY = 1
    CONST_MID_KEY = 2
    CONST_RIGHT_KEY = 3
    keep_going = True

    def init(cls):
        # Keep track of the latest click event
        cls.last_event = 0

    @classmethod
    def getClickEvent(cls):
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                cls.keep_going = False

		for event in pygame.event.get():

			# if event.type == pygame.QUIT:
			# 	cls.keep_going = False

			# if event.type == pygame.MOUSEBUTTONDOWN:
			# 	button_pressed = event.dict['button']
			# 	click = event.dict['pos']
			# 	if button_pressed == 1:  # Left click
			# 		cls.button_type = "left"
			# 		cls.click_pos = click
			# 	elif button_pressed == 3:  # Right click
			# 		cls.button_type = "right"
			# 		cls.click_pos = click
			if event.type == pygame.MOUSEBUTTONUP:
				return cls.__key_type(event.dict['button']), cls.__key_pos(event.dict['pos'])
			

			# button_released = event.dict['button']
			# click = event.dict['pos']
			# if button_released == 1:  # Left click
			# 	cls.button_type = ""
			# 	cls.click_pos = (0, 0)
			# elif button_released == 3:  # Right click
 			# 	cls.button_type = ""
			# 	cls.click_pos = (0, 0)
			# print cls.click_pos, cls.button_type

		# return cls.click_pos, cls.button_type
            elif event.type == pygame.MOUSEBUTTONDOWN:
                print cls.__key_type(event.dict['button']), cls.__key_pos(event.dict['pos'])
                return cls.__key_type(event.dict['button']), cls.__key_pos(event.dict['pos'])
            else:
                # return "", (0, 0)
                return None
	
    @classmethod
    def clicked(cls):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP:
                cls.last_event = event
                return True
            else: return False

    @classmethod
    def lastClickEvent(cls):
        return cls.last_event

    @classmethod
    def isLeftKeyClicked(cls, button):
        """Return true when left clicked a registrated button"""
        click_event = cls.getClickEvent()
        if click_event is None:
            return False
        else:
            return cls.__isMouseOnButton(button, click_event[1]) and \
                    click_event[0] == "left"

    @classmethod
    def isMidKeyClicked(cls, button):
        """Return true when mid clicked a registrated button"""
        click_event = cls.getClickEvent()
        return cls.__isMouseOnButton(button, click_event[1]) and \
               click_event[0] is None

    @classmethod
    def isRightKeyClicked(cls, button):
        """Return true when right clicked a registrated button"""
        click_event = cls.getClickEvent()
        return cls.__isMouseOnButton(button, click_event[1]) and \
               click_event[0] == "right"

    @classmethod
    def __isMouseOnButton(cls, button, click_pos):
        return cls.__mouseInWidth(button, click_pos[0]) and cls.__mouseInHeight(button, click_pos[1])

    @classmethod
    def __mouseInWidth(cls, button, click_pos_x):
        return (button.x() <= click_pos_x <= button.x()+button.width())

    @classmethod
    def __mouseInHeight(cls, button, click_pos_y):
        return (button.y() <= click_pos_y <= button.y()+button.height())

    @classmethod
    def __isLeftKey(cls, event_type):
        return event_type == cls.CONST_LEFT_KEY

    @classmethod
    def __isMidKey(cls, event_type):
        return event_type == cls.CONST_MID_KEY

    @classmethod
    def __isRightKey(cls, event_type):
        return event_type == cls.CONST_RIGHT_KEY

    @classmethod
    def __key_type(cls, pyg_button_type):
        if cls.__isLeftKey(pyg_button_type):
            return "left"
        elif cls.__isRightKey(pyg_button_type):
            return "right"

    @classmethod
    def __key_pos(cls, pyg_key_pos):
        return pyg_key_pos



