# This class defines a counter for counting mines in minesweeper

import pygame

class Counter(object):
	"""Display how many mines are left."""
	
	def __init__(self):
		self.DEFAULT_MINE_NUM = 99
		self.mine = self.DEFAULT_MINE_NUM

	def addOne(self):
		self.mine += 1

	def decreaseOne(self):
		self.mine -= 1

	def set(self, n):
		self.mine = n

	def reset(self):
		self.mine = self.DEFAULT_MINE_NUM