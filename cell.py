# The class holds information of a single cell in minesweeper.

import pygame
from metabutton import MetaButton
from mouselistener import MouseListener
# from exceptions import InvalidHintValue

class Cell(MetaButton):
	CONST_MINE_VAL 	  	 = -1
	CONST_EMPTY_VAL 	 = 0
	CONST_HINT_VALS 	 = [1,2,3,4,5,6]
	CONST_MAX_VALID_HINT = len(CONST_HINT_VALS) - 1

	def __init__(self, pos, intern_pos, size, covered_img, empty_tile_img, flag_img, mine_img, explode_img, hint_img_lst):
		self.__pos 		     = pos
		self.__internpos	 = intern_pos
		self.__size          = size
		self.__value         = Cell.CONST_EMPTY_VAL	# Enforces using high level method to set cell value, default set to empty tile.
		self.__coveredLook   = covered_img
		self.__emptyTileLook = empty_tile_img
		self.__mineImg		 = mine_img
		self.__explodeImg	 = explode_img
		self.__hintImgLst	 = hint_img_lst
		self.__appearance    = self.__coveredLook
		self.__flagImg       = flag_img
		self.__flaggedLook   = self.__flagImg
		self.__flagged       = False
		self.__locked        = False
		self.__covered       = True

	def isRevealed(self):
		return self.__covered
	
	def __getRevealedLook(self):
		if self.isMine():
			return self.__mineImg
		elif self.isEmptyTile():
			return self.__emptyTileLook
		elif self.isHint():
			hintIndex = self.isHint() - 1
			return self.__hintImgLst[hintIndex]

	def leftclick(self):
		"""
		Reveal the cell to player. Change cell's appearance, make it uncovered, make it
		unclickable, and return the cell's value and position as a tuple.
		"""
		self.__covered    = False
		self.__locked     = True
		self.__appearance = self.__getRevealedLook()

		return self.__pos, self.__value

	def midclick(self):
		"""Not needed."""
		pass

	def rightclick(self):
		"""
		Lock the cell and update the appearance to __flaggedLook if cell is not __flagged
		Unlock the cell and update the appearance to __coveredLook if cell is flaggedd
		"""
		if self.__flagged:
			self.__locked     = not (self.__locked)
			self.__flagged    = not (self.__flagged)
			self.__appearance = self.__coveredLook
		else:
			self.__locked     = True
			self.__flagged    = True
			self.__appearance = self.__flaggedLook


	def setAsMine(self):
		"""
		Sets a cell as a mine. Must only used to initialize cell value. Forbids in-game call.
		"""
		self.__value = Cell.CONST_MINE_VAL

	def setAsHint(self, hint_val):
		"""
		Sets a cell as a hint number. Must only used to initialize cell value. Forbids in-game call.
		"""
		if Cell.CONST_MINE_VAL <= hint_val <= Cell.CONST_HINT_VALS[Cell.CONST_MAX_VALID_HINT]:
			self.__value = Cell.CONST_HINT_VALS[hint_val-1]
		# else:
		# 	raise InvalidHintValue(hint_val)

	def getAbsPos(self):
		return self.__pos

	def getInternPos(self):
		return self.__internpos

	def draw(self):
		"""
		Untested Method. Not sure whether it works. 
		Might need consistently update image to show the same appearance.
		"""
		imagerect = self.__appearance.get_rect()
		screen.blit(myimage, imagerect)
		pygame.display.flip()
		
	def raisedFlag(self):
		return self.__covered and self.__flagged
	
	def downedFlag(self):
		return (not self.raisedFlag())
	
	def lock(self):
		self.__locked = True
	
	def unlock(self):
		self.__locked = False
	
	def isMine(self):
		return self.__covered and (self.__value == Cell.CONST_MINE_VAL)

	def __minHintVal(self):
		return Cell.CONST_HINT_VALS[0]

	def __maxHintVal(self):
		return Cell.CONST_HINT_VALS[Cell.CONST_MAX_VALID_HINT]

	def isHint(self):
		if self.__minHintVal() <= self.__value <= self.__maxHintVal():
			return self.__value

	def isEmptyCell(self):
		return self.__value == Cell.CONST_EMPTY_VAL