# This class defines a timer for minesweeper

import pygame

class Timer(object):

	def __init__(self):
		self.clock = pygame.time.Clock()

	def tick(self): # called every frame
		self.clock.tick()

	def get_tick(self):
		return self.clock.get_time()

	def stop(self): # not needed
		pass

	def reset(self):
		self.clock = pygame.time.Clock()

	def draw(self):
		pass