#
# I affirm that I have carried out the attached academic endeavors with full academic honesty, in
# accordance with the Union College Honor Code and the course syllabus.
# @author Yunmeng Li
# @version 1/26/2017

import pygame
from button import ControlButton
from mouselistener import MouseListener

class RunTest(object):

    width = 320
    height = 460
    my_win = pygame.display.set_mode((width, height))
    button = pygame.sprite.Group()

    @classmethod
    def init(cls):
        pygame.init()
        cls.mouse = MouseListener()
        cls.curr_button = ControlButton((120, 20), 80, 80)
        cls.button.add(cls.curr_button)

    @classmethod
    def handle_events(cls):

        keep_going = True
        #cls.mouse.getClickEvent()
        cls.curr_button.leftclick()
        keep_going = cls.mouse.keep_going
        return keep_going

    @classmethod
    def draw(cls):
        # Draw Background
        cls.my_win.fill(pygame.color.Color("LightGrey"))
        #pygame.draw.rect(cls.my_win,(165,165,165),())
        cls.button.draw(cls.my_win)
        # Swap display
        pygame.display.update()

    @classmethod
    def quit(cls):
        pygame.quit()

    @classmethod
    def run(cls):

        frame_rate = 240
        tick_time = int(1.0 / frame_rate * 1000)

        # The game loop starts here.
        keep_going = True
        while keep_going:

            keep_going = cls.handle_events()

            pygame.time.wait(tick_time)

            cls.draw()

        cls.quit()


RunTest.init()
RunTest.run()